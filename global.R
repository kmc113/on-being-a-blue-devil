# This file is the first one run when compiling the Shiny app, so here is where we can
# load all of our necessary libraries and read in our data frames from CSVs.

library(shiny)
library(shinythemes)
library(choroplethr)
library(choroplethrMaps)
library(tidyverse)
library(ggrepel)
library(gapminder)
library(numbers)
library(stringr)
library(socviz)
library(maps)
library(viridis)
library(RColorBrewer)
library(dplyr)

midCenDataKat6974 <- read.csv("DF_1969F-1974S-consistent-V2.csv")
data2000s <- read.csv("Aggregate_enrollment_data-cleaned - Aggregate_enrollment_data-cleaned2.csv")

# We can clean this code up as we work to create a more centralized input system

data2000s <- data2000s[-1]

# Note--different data frame structures in midCenDataKat6974 and data2000s


# This data frame is used simply for demonstration purposes on this file:
map_data <- data2000s %>% 
  filter(state_or_country == "state", academic_year == 2000) %>%
  mutate(region = tolower(home_loc)) %>%
  group_by(region)
  # filter(substring(term_descr, 6, 11) == "Spring")
# Note--2000 has no fall semester here. For consistency and accuracy, we're using Spring ***

# Note: region and total columns necessary for choroplethr functions

map_data_numeric <- map_data[, -c(1:6, 27)]
# This is all the columns of map_data that contain numbers of enrollment
# (i.e. no region, home_loc, term_desc, etc.)
