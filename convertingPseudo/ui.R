# This is the UI file for the Pseudo Shiny application


fluidPage(
  navbarPage("Blue Devil Breakdown",
             tabPanel("Home", fluid = TRUE,
                      fluidRow(
                        h1(p("Welcome to the landing page!")),
                        h5(p("This page might have some brief introductory text, maybe some historical Duke pics and summary visualizations.")),
                        br(),
                        h4(p("This is a map!:")),
                        img(src = "decade_percentages.png", height = 353, width = 566),
                        h3(p("Informative!"))
                      ) # Close fluidRow
             ), # Close "Home" tabPanel
             tabPanel(
                    "Explore the Data",
                      #div(#class = "outer",
                      # Example links to CSS and JS files (for custom aesthetics and ability to jump to map from chart). We can add later as needed.
                      leafletOutput("map", width = "100%", height = 450),
                      absolutePanel(id = "controls", class = "panel panel-default", fixed = TRUE, draggable = TRUE, top = 60, left = "auto", right = 20,
                                    bottom = "auto", width = "300", height = "450",
                                    
                                    #h2("Header 2"),
                                    numericInput(
                                      inputId =  "date", 
                                      label = "Input Year:", 
                                      value = 1975, min = 1970, max = 1979
                                    ),
                                    selectInput("term", "Term", termVars),
                                    selectInput("school", "School", schoolVars),
                                    selectInput("color", "Compare 1", lookingAt),
                                    selectInput("size", "Compare 2", lookingAt),
                                    # Above is intended to display the elements that had not been chosen in the first selection. It may or may not work.
                                    # Example also included 'selected = *NAME*', which I don't understand bc variable already seems to be assigned to size.
                                    
                                    plotOutput("stateBar", height = 200),
                                    plotOutput("scatterVarByVar", height = 250)
                      )
                      
                      #tags$div(id = "cite",
                      #'Data sourced from the Duke University Archivist and Duke Libraries'
                      
                      
                      #)
                  
                      
             ), # Close "Explore the data" tabPanel
             navbarMenu("History of Duke",
                        tabPanel("The School", fluid = TRUE,
                                 fluidRow(
                                   h1(p("History of Duke University")),
                                   img(src = "Duke_University_Schools_Over_Time-1.png",
                                       height = 1152/2, width = 1728/2),
                                   br(),
                                   h3(p("Maybe a paragraph")),
                                   p("Aboud Duke's long and complicated history as a school of schools"),
                                   h3(p("And a timeline would be awesome!")),
                                   p("Timeline here"),
                                   br(),
                                   h3(p("Outgoing links")),
                                   p("List of outgoing links here")
                                 ) # Close fluidRow
                        ), # Close "The School" tabPanel
                        tabPanel("Race at Duke", fluid = TRUE,
                                 fluidRow(
                                   h1(p("Race at Duke University")),
                                   img(src = "Allen_building.jpg",
                                       height = 632/2, width = 1000/2),
                                   br(),
                                   h3(p("Maybe a paragraph")),
                                   p("Aboud Duke's long and complicated history with race"),
                                   h3(p("And a timeline would be awesome!")),
                                   p("Timeline here"),
                                   br(),
                                   h3(p("Outgoing links")),
                                   p("List of outgoing links here")
                                 ) # Close fluidRow
                        ), # Close "Race at Duke" tabPanel
                        tabPanel("LGBTQ at Duke", fluid = TRUE,
                                 fluidRow(
                                   h1(p("LGBTQ at Duke")),
                                   img(src = "lgbt_duke.jpg",
                                       height = 265, width = 699),
                                   br(),
                                   h3(p("Maybe a paragraph")),
                                   p("About Duke's short and minimal history with LGBT presence"),
                                   h3(p("And a timeline would be awesome!")),
                                   p("Timeline here"),
                                   br(),
                                   h3(p("Outgoing links")),
                                   p("List of outgoing links here")
                                 ) # Close fluidRow
                        ) # Close "LGBTQ at Duke" tabPanel
             ), # Close "History of Duke" navbarMenu
             navbarMenu("About",
                        tabPanel("About the project", fluid = TRUE,
                                 fluidRow(
                                   h1(p("About the project")),
                                   strong("Project Summary"),
                                   p("A team of students, led by University Archivist Valerie Gillispie and Professor Don Taylor, will take a closer look at how the student body at Duke has transformed into a coeducational student body from around the world enrolled in ten different schools.")
                                 ) # Close fluidRow
                        ), # Close "About the project" tabPanel
                        tabPanel("About the data", 
                                 sidebarLayout(
                                   sidebarPanel(
                                     h4(p("Maybe some toggles here"))
                                   ), # Close sidebarPanel
                                   mainPanel(
                                     fluidRow(
                                       h1(p("About the data")),
                                       strong("Short Intro"),
                                       p("We have data from this, that, and the other thing!"),
                                       h3(p("Interact with the data!")),
                                       DT::dataTableOutput("IrisTable"),
                                       br(),
                                       h3(p("See for yourself: Original documents")),
                                       img(src = "1961Sscanned.png",
                                           height = 799, width = 597),
                                       img(src = "UG-1964S.PNG",
                                           height = 796, width = 613)
                                     ) # Close fluidRow
                                   ) # Close mainPanel
                                 ), # Close sidebarLayout
                        ), # Close "About the data" tabPanel
                        tabPanel("Researcher experiences", fluid = TRUE,
                                 fluidRow(
                                   h1(p("Researcher experiences")),
                                   br(),
                                   h3("Researcher experience: Elizabeth Barahona"),
                                   p("Elizabeth Barahona is a first-year doctoral student in history at Northwestern University ... blah blah blah"),
                                   p("This is a video!  Just kidding."),
                                   img(src = "EB-UserStory.PNG",
                                       height = 363, width = 650)
                                 ) # Close fluidRow
                        ), # Close "Researcher experiences" tabPanel
                        tabPanel("Meet the team", fluid = TRUE,
                                 fluidRow(
                                   h1(p("Meet the team")),
                                   br(),
                                   h3("Don Taylor"),
                                   p("Who Don Taylor is, image?"),
                                   p("Relationship to project."),
                                   br(),
                                   h3("Valerie Gillispie"),
                                   p("Who Valerie Gillispie is, image?"),
                                   p("Relationship to project."),
                                   br(),
                                   h3("Anna Holleman"),
                                   p("Who Anna Holleman is, image?"),
                                   p("Relationship to project."),
                                   br(),
                                   h3("Alex Burgin"),
                                   p("Who Alex Burgin is, image?"),
                                   p("Relationship to project."),
                                   br(),
                                   h3("Kat Cottrell"),
                                   p("Who Kat Cottrell is, image?"),
                                   p("Relationship to project."),
                                   br(),
                                   h3("Michaela Kotarba"),
                                   p("Who Michaela Kotarba is, image?"),
                                   p("Relationship to project."),
                                   br(),
                                   h3("Special thanks to ..."),
                                   p("Data+ staff, Anna K, Elizabeth B, etc."),
                                   br(),
                                   br(),
                                   br(),
                                   br()
                                 ) # Close fluidRow
                        ), # Close "Meet the team" tabPanel
                        tabPanel("FAQ",
                                 # Tab 3 stuff
                                 sidebarLayout(
                                   sidebarPanel(
                                     titlePanel("FAQ Sidebar"),
                                     h5(p("Maybe a table of contents here?")),
                                     br(),
                                     strong("Campus layout"),
                                     br(),
                                     strong("Race, Sex, & Gender"),
                                     br(),
                                     strong("Years, Semesters, and Dates")
                                   ), # Close sidebarPanel
                                   mainPanel(
                                     fluidRow(
                                       h1("Frequently Asked Questions"),
                                       br(),
                                       h3("Campus layout"),
                                       p(span(strong("QUESTION:  ")),
                                         span("Is that the Chapel?", style = "font-style:italic")),
                                       p(span(strong("ANSWER:  ")),
                                         "No.  That is Perkins Library."),
                                       br(),
                                       p(span(strong("QUESTION:  ")),
                                         span("How do I get to the Chapel?", style = "font-style:italic")),
                                       p(span(strong("ANSWER:  ")),
                                         "It is so tall.  You cannot possibly miss it."),
                                       br(),
                                       br(),
                                       h3("Race, Sex, & Gender"),
                                       br(),
                                       br(),
                                       h3("Years, Semesters, and Dates")
                                     ) # Close fluidRow
                                   ) # Close mainPanel
                                 ) # Close sidebarLayout
                        ) # Close "FAQ" tabPanel
             ), # Close "About" navbarMenu
             tags$style(type = 'text/css', 
                        '.navbar {background-color: #012169;}',
                        '.navbar-default .navbar-brand{color: white;
                        font-size: 30px; font-family: "EB Garamond"}'
             ) # Close tags$style
  ) # Close navbarPage
) # Close ui








#-------------------------------BEGIN INTERACTIVE MAP UI---------------------------------------------------
#' 
#' schoolVars <- c("All" = "All.Schools", "All Graduate Schools" = "allGrad", "All Undergraduate Schools" = "allUndergrad", "Trinity" = "Trinity", "Nursing" = "Nursing",
#'                 "Graduate" = "Graduate", "Divinity" = "Divinity", "Law" = "Law", "Business" = "Business", "Engineering" = "Engineering", "Environment" = "Environment", "Medicine" = "Medicine")
#' # Clarification will be needed for users; Graduate school needs to be differentiated from graduate schools en masse, Nursing needs to be shown to include both School of Nursing and
#' # graduate nursing, likewise with engineering
#' 
#' lookingAt <- c("Total Enrollment" = "total", "Percentage Change in Enrollment" = "percentChange", "Percent Female" = "percentFemale", "Enrollment per Capita" = "totalPerCap")
#' # More variables can be introduced as we refine our code methodology. Each variable (with the exception of Total Enrollment) will need to be calculated and present in a data frame
#' # which can (and should) be done on the global.R page. Some will require merging megamaster with outside data sets. And Percentage Change in Enrollment will need to have a specified
#' # time frame.
#' fluidPage(
#' navbarPage("Navigation Page", id = "nav",
#'            tabPanel("Interactive Map",
#'                   #div(#class = "outer",
#'                     # Example links to CSS and JS files (for custom aesthetics and ability to jump to map from chart). We can add later as needed.
#'                       leafletOutput("map", width = "100%", height = 1000),
#'                       absolutePanel(id = "controls", class = "panel panel-default", fixed = TRUE, draggable = TRUE, top = 60, left = "auto", right = 20,
#'                                     bottom = "auto", width = 330, height = "auto",
#' 
#'                                     h2("Header 2"),
#'                                     selectInput(
#'                                       inputId =  "date", 
#'                                       label = "Select time period:", 
#'                                       choices = 1970:1980
#'                                     ),
#'                                     
#'                                     selectInput("school", "School", schoolVars),
#'                                     selectInput("color", "Compare 1", lookingAt),
#'                                     selectInput("size", "Compare 2", lookingAt),
#'                                     # Above is intended to display the elements that had not been chosen in the first selection. It may or may not work.
#'                                     # Example also included 'selected = *NAME*', which I don't understand bc variable already seems to be assigned to size.
#' 
#'                                     plotOutput("stateBar", height = 200),
#'                                     plotOutput("scatterVarByVar", height = 250)
#'                                    )
#' 
#'                     #tags$div(id = "cite",
#'                              #'Data sourced from the Duke University Archivist and Duke Libraries'
#' 
#' 
#'                       #)
#' 
#'                     ) #close tabPanel
#'            # Here is where we can introduce additional tabPanels as we get farther along
#' 
#' )
#' )

#--------------------------END INTERACTIVE MAP UI-----------------------------------------------------------







