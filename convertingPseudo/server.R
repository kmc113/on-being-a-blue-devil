# This is the server file for the Pseudo Shiny application


#filter dataset by chosen year


#create choropleth map characterized by chosen school's enrollment
function (input, output, session) {
#filteredData <- reactive({ megamasterB #%>%
  #filter (Year == input$year)
  #})
  
  server <- function(input, output) {
    output$IrisTable = DT::renderDataTable({
      iris
    })
  } # Close server
  
  
  
  output$map <- renderLeaflet({
    # leaflet(wmap) %>%
    #   setView(-96, 37.8, 4) %>%
    #   addTiles() 
    #can use URL template here
      #addPolygons(megamasterB, lng = ~lng, lat = ~lat, fillColor = input$school,
                  #fillOpacity = 0.7) #%>%
      # addLegend(pal = ~pal, values = (input$school),
      #           position = "topright", title = Enrollment)
    
    leaflet() %>%
      setView(0, 37.8, 1.5) %>%
      #maxbounds 
      addTiles() %>% 
      clearShapes() #%>% 
      # addPolygons(fillColor = wmap$All.Schools,
      #             weight = 2,
      #             opacity = 1,
      #             color = "gray",
      #             dashArray = "3",
      #             fillOpacity = 0.7,
      #             highlight = highlightOptions(
      #               weight = 5,
      #               color = "white",
      #               dashArray = "3",
      #               fillOpacity = .8,
      #               bringToFront = TRUE)
      # )
    
    
    
    
    
  })

#can create histogram displays here based on wants

#Layer circles on top of choropleth -

# observe({
#    colorBy <- input$color
#    sizeBy <- input$size
# #choose correct column for color and size
#    colorData <- filteredData$colorBy
#    radius <- filteredData$sizeBy
# 
#   #update with circles
#   leafletProxy("map", data = filteredData) %>%
#     clearShapes() %>%
#     addCircles(~longitude, ~latitude, radius=radius,stroke=FALSE,
#                fillOpacity=0.4, fillColor=pal(colorData)) %>%
#     addLegend("bottomleft", pal=~pal, values=colorData, title=colorBy,
#               layerId="colorLegend")
# 
# })
  
#world map choropleth-- this is when you hover over countries on the map
observe({
schoolInput <- input$school

wmap <- joinCountryData2Map(megamasterB %>% filter(Year == input$date) %>% filter(Semester == input$term)
                            %>% group_by(Origin), joinCode = "ISO3", nameJoinColumn = "ISO3")
#summarise by input school

wmap$Trinity[is.na(wmap$Trinity)] <- 0

#take out NA's for each school (for i in n cols dataframe)
wmap$Nursing[is.na(wmap$Nursing)] <- 0
wmap$Engineering[is.na(wmap$Engineering)] <- 0
wmap$Graduate[is.na(wmap$Graduate)] <- 0
wmap$Divinity[is.na(wmap$Divinity)] <- 0
wmap$Law[is.na(wmap$Law)] <- 0
wmap$Environment[is.na(wmap$Environment)] <- 0
wmap$Business[is.na(wmap$Business)] <- 0
wmap$Medicine[is.na(wmap$Medicine)] <- 0

pal <- colorNumeric(palette = "GnBu", domain = wmap$schoolInput)
    if(!is.null(input$school)){

      leafletProxy("map", data = wmap) %>%
        addTiles() %>%
        clearShapes() %>%
        addPolygons(fillColor = ~pal(wmap[[schoolInput]]*3),
                    weight = 2,
                    opacity = 1,
                    color = "white",
                    dashArray = "3",
                    fillOpacity = 0.7,
                    highlight = highlightOptions(
                      weight = 5,
                      color = "white",
                      dashArray = "3",
                      fillOpacity = .8,
                      bringToFront = TRUE)
                    )
    }})
}
